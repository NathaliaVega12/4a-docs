const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');
class ComentarioAPI extends RESTDataSource {
constructor() {
super();
this.baseURL = serverConfig.comentario_api_url;
}
async createComentario(comentario) {
comentario = new Object(JSON.parse(JSON.stringify(comentario)));
return await this.post('/registrar', comentario);
}
async comentariosByUsername(username) {
return await this.get(`/buscarusuario/${username}`);
}
async eliminarById(id) {
return await this.delete(`/eliminarcomentario/${id}`);
}

}
module.exports = ComentarioAPI;