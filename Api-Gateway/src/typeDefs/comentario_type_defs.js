const { gql } = require('apollo-server');

const comentarioTypeDefs = gql `
    type Account {
        id : String!
        username : String!
        lugar : String!
        fecha : Date!
        hora : time!
        comentario : String!
    }

    extend type Query {
        comentarioByUsername(username: String!): Comentario
    }
`;
module.exports = accountTypeDefs;