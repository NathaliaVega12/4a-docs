package com.biciapp.biciapp.repositories;

import com.biciapp.biciapp.models.Comentario;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ComentarioRepository extends MongoRepository<Comentario,String>{
    List<Comentario> findByUsername(String username);
}
