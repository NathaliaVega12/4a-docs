package com.biciapp.biciapp.controllers;

import com.biciapp.biciapp.models.Comentario;
import com.biciapp.biciapp.exceptions.ComentarioNotFoundException;
import com.biciapp.biciapp.repositories.ComentarioRepository;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController

public class ComentarioController {

    private final ComentarioRepository comentarioRepository;

    public ComentarioController (ComentarioRepository comentarioRepository){
        this.comentarioRepository=comentarioRepository;
    }

    @GetMapping("/buscarusuario/{username}")
    List<Comentario> getComentario(@PathVariable String username) {
        List<Comentario> comentarioxusuario = comentarioRepository.findByUsername(username);
        if (comentarioxusuario==null){
            System.out.print("No se encontraron comentarios con el username:" + username);
        }
        return comentarioxusuario;
    }

    @PostMapping("/registrar")
    Comentario newComentario(@RequestBody Comentario comentario){
        return comentarioRepository.save(comentario);
    }

    @DeleteMapping("/eliminarcomentario/{id}")
    Comentario deleteComentario(@PathVariable String id){
        Comentario comentario1=comentarioRepository.findById(id).orElse( null);
        if (comentario1==null)
            throw new ComentarioNotFoundException("No se encontró comentario con el Id:" +id);
        comentarioRepository.delete(comentario1);
        return comentario1;
    }
    }

