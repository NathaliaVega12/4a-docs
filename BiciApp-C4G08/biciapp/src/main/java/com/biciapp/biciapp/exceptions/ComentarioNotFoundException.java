package com.biciapp.biciapp.exceptions;

public class ComentarioNotFoundException extends RuntimeException{
    public ComentarioNotFoundException(String message) {
        super(message);
    }
}
