package com.biciapp.biciapp.models;

import org.springframework.data.annotation.Id;
import java.util.Date;
import java.time.LocalTime;

public class Comentario {
    @Id
    private String id;
    private String username;
    private String lugar;
    private Date fecha;
    private LocalTime hora;
    private String comentario;

    public Comentario(String id, String username, String lugar, Date fecha, LocalTime hora, String comentario) {
        this.id = id;
        this.username = username;
        this.lugar = lugar;
        this.fecha = fecha;
        this.hora = hora;
        this.comentario = comentario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public LocalTime getHora() {
        return hora;
    }

    public void setHora(LocalTime hora) {
        this.hora = hora;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
}
