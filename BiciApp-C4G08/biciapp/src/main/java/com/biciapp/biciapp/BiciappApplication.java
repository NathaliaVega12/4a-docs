package com.biciapp.biciapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BiciappApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiciappApplication.class, args);
	}

}
